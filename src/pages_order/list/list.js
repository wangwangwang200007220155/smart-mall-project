// pages_order/checkout/checkout.js
Page({
  data: {
    // tab栏数据
    tabs: [
      { id: '1', title: '全部', type: 'all', isRender: false },
      { id: '2', title: '待支付', type: 'payment', isRender: false },
      { id: '3', title: '待发货', type: 'delivery', isRender: false },
      { id: '4', title: '待收货', type: 'received', isRender: false },
      { id: '5', title: '待评价', type: 'comment', isRender: false },
    ],
  },
})
