import http from 'wechat-http'

// 请求基地址
http.baseURL = 'https://smart-shop.itheima.net/index.php?s=/api'

// 请求拦截器
http.intercept.request = (config) => {
  // 拦截器处理后的请求参数
  return config
}

// 响应拦截器
http.intercept.response = (result) => {
  // 拦截器处理后的响应结果
  return result.data
}

// 默认导出
export default http
