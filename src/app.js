// app.js
App({
  onLaunch() {
    // 小程序启动时，检查更新
    this.checkUpdate()
  },
  checkUpdate() {
    // 获取版本更新管理器
    const updateManager = wx.getUpdateManager()

    // 检查更新结果
    updateManager.onCheckForUpdate(function (res) {
      // 是否有更新
      console.log('🟢 小程序是否有更新', res.hasUpdate)
    })

    // 监听小程序有版本更新事件
    updateManager.onUpdateReady(function () {
      // 客户端主动触发下载（无需开发者触发下载，只需要做引导重启的提示即可）
      wx.showModal({
        title: '更新提示',
        content: '新版本已经准备好，是否重启应用？',
        success: function (res) {
          if (res.confirm) {
            // 新的版本已经下载好，调用 applyUpdate 应用新版本并重启
            updateManager.applyUpdate()
          }
        },
      })
    })
  },
})
